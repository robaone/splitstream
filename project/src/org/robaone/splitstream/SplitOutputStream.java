package org.robaone.splitstream;

import java.io.IOException;
import java.io.OutputStream;

public class SplitOutputStream extends OutputStream {

	private SplitWriteHandler m_split_handler;
	public SplitOutputStream(SplitWriteHandler handler){
		this.m_split_handler = handler;
	}
	@Override
	public void write(int b) throws IOException {
		m_split_handler.write(b);
	}
	public int size() {
		return this.m_split_handler.size();
	}

}
