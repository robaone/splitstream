package org.robaone.splitstream;

import java.io.IOException;

public interface SplitWriteHandler {
	void write(int b) throws IOException;

	int size();

	
}
