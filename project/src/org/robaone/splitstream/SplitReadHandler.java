package org.robaone.splitstream;

import java.io.IOException;

public interface SplitReadHandler {

	int read() throws IOException;

}
