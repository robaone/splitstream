package org.robaone.splitstream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

abstract public class SplitOutputStreamHandler<O extends OutputStream> implements SplitWriteHandler {
	ArrayList<O> split = new ArrayList<O>();
	int size = 0;
	int read_index = 0;
	boolean read_started = false;
	private int m_max_size;
	public SplitOutputStreamHandler(int size){
		this.m_max_size = size;
	}
	@Override
	public void write(int b) throws IOException {
		if(size == 0){
			O bout = getNextOutputStream();
			split.add(bout);
		}
		size++;
		if(size > getMaxSize()){
			O bout = getNextOutputStream();
			split.add(bout);
			size = size - getMaxSize();
		}
		split.get(split.size()-1).write(b);
	}

	abstract protected O getNextOutputStream();
	protected int getMaxSize() {
		return m_max_size;
	}

	@Override
	public int size(){
		return split.size();
	}
	
}
