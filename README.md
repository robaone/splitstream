# Split Stream

Input and Output streams that support splitting a file into multiple smaller pieces during writing and during reading.

The resting nature of the data should be transparent to the class that will be reading and writing to the stream.
