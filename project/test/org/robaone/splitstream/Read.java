package org.robaone.splitstream;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class Read {
	SplitOutputStream out;
	SplitInputStream in;
	@Before
	public void setUp() throws Exception {
		SplitByteArrayHandler handler = new SplitByteArrayHandler(54);
		out = new SplitOutputStream(handler);
		in = new SplitInputStream(handler);
	}

	@Test
	public void testWriteByteArray() {
		byte[] input = new String("This is a large string that can be used to create bytes").getBytes();
		assertEquals("byte length",55,input.length);
		
		try {
			out.write(input);
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			for(int i = in.read();i > -1;i = in.read()){
				bout.write(i);
			}
			assertEquals("split read",new String(input),bout.toString());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testGenericWrite() {
		byte[] input = new String("This is a large string that can be used to create bytes").getBytes();
		
		SplitOutputStream out = new SplitOutputStream(new SplitOutputStreamHandler<ByteArrayOutputStream>(10){

			@Override
			protected ByteArrayOutputStream getNextOutputStream() {
				return new ByteArrayOutputStream();
			}
			
		});
		
		try{
			out.write(input);
			assertEquals("split size",6,out.size());
		}catch(IOException e){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testGenericRead() {
		SplitInputStream in = new SplitInputStream(new SplitInputStreamHandler<ByteArrayInputStream>(2){
			byte[] part1 = new String("Hello ").getBytes();
			byte[] part2 = new String("World!").getBytes();
			boolean next = true;
			@Override
			protected ByteArrayInputStream getNextInputStream() {
				next = !next;
				return new ByteArrayInputStream(next ? part2 : part1);
			}
			
		});
		
		try{
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			for(int i = in.read();i > -1;i = in.read()){
				bout.write(i);
			}
			String result = bout.toString();
			assertEquals("read test","Hello World!",result);
		}catch(IOException e){
			fail(e.getMessage());
		}
	}

}
