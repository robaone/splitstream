package org.robaone.splitstream;

import java.io.IOException;
import java.io.InputStream;

public class SplitInputStream extends InputStream {
	private SplitReadHandler m_split_handler;
	public SplitInputStream(SplitReadHandler handler){
		this.m_split_handler = handler;
	}
	@Override
	public int read() throws IOException {
		return this.m_split_handler.read();
	}

}
