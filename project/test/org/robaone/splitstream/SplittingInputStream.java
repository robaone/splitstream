package org.robaone.splitstream;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.robaone.dbase.ConnectionBlock;
import com.robaone.dbase.DBManager;
import com.robaone.dbase.HDBConnectionManager;
import com.robaone.util.LineReader;

public class SplittingInputStream {
	InputStream in;
	String TEST_STRING = "This is a test string that must be broken up";
	@Before
	public void setUp() throws Exception {
		in = new ByteArrayInputStream(TEST_STRING.getBytes());
		new ConnectionBlock(){

			@Override
			protected void run() throws Exception {
				String str = "delete from binary_data where idbinary_data is not null";
				this.prepareStatement(str);
				this.executeUpdate();
			}

		}.run(this.getConnectionManager());
	}

	int index = -1;
	@Test
	public void testGo() {
		SplitSQLInputStreamHandler splitter = new SplitSQLInputStreamHandler(10){
			PreparedStatement ps;
			@Override
			protected
			PreparedStatement prepareStatement(Connection con) throws Exception {
				index++;
				String str = "insert into binary_data (name,data) values (?,?)";
				ps = con.prepareStatement(str);
				return ps;
			}

			@Override
			protected
			void setStreamParameter(InputStream input) throws Exception {
				ps.setString(1, "item"+index);
				ps.setBinaryStream(2, input);
			}

			@Override
			protected
			HDBConnectionManager getConnectionManager() {
				return SplittingInputStream.this.getConnectionManager();
			}

		};

		try {
			splitter.go(in);
			assertEquals("Parts",6,index+1);
			new ConnectionBlock(){

				@Override
				protected void run() throws Exception {



					SplitInputStreamHandler<InputStream> handler = new SplitInputStreamHandler<InputStream>(index+1){
						int m_current_index = 0;
						ArrayList<PreparedStatement> m_ps = new ArrayList<PreparedStatement>();
						ArrayList<ResultSet> m_rs = new ArrayList<ResultSet>();
						@Override
						protected InputStream getNextInputStream() {

							try{
								String str = "select data from binary_data where name = ?";
								PreparedStatement ps = getConnection().prepareStatement(str);
								ps.setString(1, "item"+m_current_index);
								ResultSet rs = ps.executeQuery();
								m_ps.add(ps);
								m_rs.add(rs);
								m_current_index++;
								rs.next();
								return rs.getBinaryStream(1);
							}catch(Exception e){
								e.printStackTrace();
								return null;
							}
						}

					};
					SplitInputStream split_in = new SplitInputStream(handler);

					LineReader lr = new LineReader(split_in);
					String str = lr.ReadLine();

					assertEquals("Read String",TEST_STRING,str);
				}

			}.run(getConnectionManager());

		} catch (Exception e) {
			fail("Error: "+e.getMessage());
			e.printStackTrace();
		}
	}

	public HDBConnectionManager getConnectionManager() {
		return new HDBConnectionManager(){

			@Override
			public Connection getConnection() throws Exception {
				String driver = System.getProperty("driver");
				String url =System.getProperty("url");
				String username=System.getProperty("username");
				String password=System.getProperty("password");
				return DBManager.getConnection(driver, url, username, password);
			}

			@Override
			public void closeConnection(Connection m_con)
					throws Exception {
				m_con.close();
			}

		};
	}

}
