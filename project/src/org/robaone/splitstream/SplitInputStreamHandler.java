package org.robaone.splitstream;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

abstract public class SplitInputStreamHandler<S extends InputStream> implements SplitReadHandler {
	ArrayList<S> insplit = new ArrayList<S>();
	int size = 0;
	int read_index = 0;
	boolean read_started = false;
	private int m_size;
	public SplitInputStreamHandler(int size){
		this.m_size = size;
	}
	protected int getSize() {
		return m_size;
	}

	@Override
	public int read() throws IOException {
		if(!read_started){
			copy();
			read_started = true;
		}
		if(this.read_index < insplit.size()){
			int retval;
			retval = insplit.get(this.read_index).read();
			if(retval == -1){
				this.read_index++;
				return this.read();
			}else{
				return retval;
			}
		}else{
			return -1;
		}
	}

	private void copy() {
		insplit.clear();
		for(int i = 0; i < this.getSize();i++){
			S bin = getNextInputStream();
			insplit.add(bin);
		}
	}
	abstract protected S getNextInputStream();
}
