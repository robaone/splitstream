package org.robaone.splitstream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class SplitByteArrayHandler implements SplitReadHandler, SplitWriteHandler {
	ArrayList<ByteArrayOutputStream> split = new ArrayList<ByteArrayOutputStream>();
	ArrayList<ByteArrayInputStream> insplit = new ArrayList<ByteArrayInputStream>();
	int size = 0;
	int read_index = 0;
	boolean read_started = false;
	private int m_max_size;
	public SplitByteArrayHandler(int size){
		this.m_max_size = size;
	}
	@Override
	public void write(int b) {
		if(size == 0){
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			split.add(bout);
		}
		size++;
		if(size > getMaxSize()){
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			split.add(bout);
			size = size - getMaxSize();
		}
		split.get(split.size()-1).write(b);
		
	}

	protected int getMaxSize() {
		return m_max_size;
	}

	@Override
	public int read() {
		if(!read_started){
			copy();
			read_started = true;
		}
		if(this.read_index < insplit.size()){
			int retval = insplit.get(this.read_index).read();
			if(retval == -1){
				this.read_index++;
				return this.read();
			}else{
				return retval;
			}
			
		}else{
			return -1;
		}
	}
	
	private void copy() {
		insplit.clear();
		for(int i = 0; i < split.size();i++){
			ByteArrayInputStream bin = new ByteArrayInputStream(split.get(i).toByteArray());
			insplit.add(bin);
		}
	}
	@Override
	public int size() {
		return split.size();
	}
}
