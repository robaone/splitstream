package org.robaone.splitstream;

import java.io.IOException;
import java.io.InputStream;

import com.robaone.api.data.AppDatabase;
import com.robaone.dbase.ConnectionBlock;
import com.robaone.dbase.HDBConnectionManager;

abstract public class SplitSQLInputStreamHandler {
	private int m_size;
	public SplitSQLInputStreamHandler(int max_size){
		this.m_size = max_size;
	}
	public void go(final InputStream in) throws Exception{
		class CountingInputStream extends InputStream{
			int byte_count = 0;
			@Override
			public int read() throws IOException {
				if(byte_count < m_size){
					int b = in.read();
					if(b != -1){
						byte_count++;
					}
					return b;
				}else{
					return -1;
				}
			}

			public int getBytesRead(){
				return byte_count;
			}

		};
		new ConnectionBlock(){
			@Override
			protected void run() throws Exception{
				CountingInputStream input = new CountingInputStream();
				this.setPreparedStatement(SplitSQLInputStreamHandler.this.prepareStatement(getConnection()));
				int current_byte_count = 0;
				SplitSQLInputStreamHandler.this.setStreamParameter(input);
				AppDatabase.writeLog("executeUpdate()");
				int updated = this.executeUpdate();
				if(updated > 0){
					current_byte_count = input.getBytesRead();
					if(current_byte_count > 0){
						go(in);
					}
				}
			}
		}.run(this.getConnectionManager());
	}

	abstract protected java.sql.PreparedStatement prepareStatement(java.sql.Connection con) throws Exception;
	abstract protected void setStreamParameter(InputStream input) throws Exception;
	abstract protected HDBConnectionManager getConnectionManager();
}
